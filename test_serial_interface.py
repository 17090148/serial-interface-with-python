# -*- coding: utf-8 -*-
"""
Created on Wed Oct 15 00:06:29 2014

@author: Dawid
"""
from serial_interface import Heartrate


    
def counter_converter(quater_minute_count):#converts the count of heart beats in 15 seconds to one minute
    """
    converts the count of heart beats in 15 seconds to one minute
    
    >>> counter_converter(-1) 
    0
    >>> counter_converter(2)
    8
    >>> counter_converter(4)
    16
    >>> counter_converter(0)
    0
    """
    if quater_minute_count < 0:
        return 0
    return quater_minute_count*4

def test_counter_converter_negative():
    print 'test_counter_converter_negative'
    assert counter_converter(-1) == 0

def test_counter_converter_4():
    'test_counter_converter_4'
    assert counter_converter(4) == 16

# -*- coding: utf-8 -*-
"""
Created on Mon Aug 18 22:56:53 2014

@author: Dawid
"""

# "Serial" is used for the COM poort
# "time" is used for the timing functions
from heartrate import Heartrate 
    
def main():
    hr = Heartrate(8)
    for i in range(0,100):
        hr.measure_rise_and_fall()
        hr.get_period_smart()
        print hr.get_heartrate()
    hr.closeser()

main()

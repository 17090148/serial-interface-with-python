# -*- coding: utf-8 -*-
"""
Created on Wed Oct 15 00:06:29 2014

@author: Dawid
"""
from heartrate import Heartrate
    
def test_add_times():
    hr = Heartrate(-1)
    hr.rising_edge_time = [0]*5
    hr.falling_edge_time = [0]*5
    assert hr.rising_edge_time[4] == 0
    assert hr.falling_edge_time[4] == 0
    hr.add_times(0,0.5)
    assert hr.rising_edge_time[4] == 0
    assert hr.falling_edge_time[4] == 0.5
    hr.add_times(1.1,1.5)
    hr.add_times(2,2.5)
    assert hr.rising_edge_time[2] == 0
    assert hr.falling_edge_time[2] == 0.5
    assert hr.rising_edge_time[4] == 2
    assert hr.falling_edge_time[4] == 2.5
    hr.add_times(3.1,3.5)
    hr.add_times(4,4.5)
    assert hr.rising_edge_time[0] == 0
    assert hr.falling_edge_time[0] == 0.5
    assert hr.rising_edge_time[4] == 4
    assert hr.falling_edge_time[4] == 4.5
    
def test_add_period():
    hr = Heartrate(-1)
    hr.period_vector = [0]*5
    hr.add_period(0.7)
    assert hr.period_vector[4] == 0.7
    hr.add_period(0.82)
    assert hr.period_vector[3] == 0.7
    hr.add_period(0.98)
    hr.add_period(0.1)
    hr.add_period(0.9)
    assert hr.period_vector[0] == 0.7
    assert hr.period_vector[1] == 0.82
    assert hr.period_vector[2] == 0.98
    assert hr.period_vector[3] == 0.1
    assert hr.period_vector[4] == 0.9
    
def test_initiate_period_vector():
    hr = Heartrate(-1)
    hr.initiate_period_vector()
    assert hr.period_vector[0] == 1
    
def test_get_period_primative():
    hr = Heartrate(-1)
    hr.rising_edge_time = [0]*5
    hr.falling_edge_time = [0]*5
    hr.max_readings = 5
    hr.add_times(0,0.5)
    hr.add_times(1.1,1.5)
    hr.add_times(2,2.5)
    hr.add_times(3,3.5)
    hr.add_times(4,4.5)
    assert hr.rising_edge_time[4] == 4
    assert hr.rising_edge_time[3] == 3
    assert hr.get_period_primative(0,1) == 1
    assert hr.get_period_primative(0,2) == 2
    assert hr.get_period_primative(2,1) == None
    assert hr.get_period_primative(3,4) == 1.1

def test_high_time():
    hr = Heartrate(-1)
    hr.rising_edge_time = [0]*5
    hr.falling_edge_time = [0]*5
    hr.max_readings = 5
    hr.add_times(0,0.5)
    hr.add_times(1.1,1.5)
    hr.add_times(2.3,2.5)
    hr.add_times(3.1,3.5)
    hr.add_times(4,4.5)
    assert hr.get_high_time(0) == 0.5
    assert round(hr.get_high_time(2),1) == 0.2
    assert round(hr.get_high_time(3),1) == 0.4

def test_get_period_smart():
    hr = Heartrate(-1)
    hr.rising_edge_time = [0]*5
    hr.falling_edge_time = [0]*5
    hr.period_vector = [0]*5
    hr.add_period(1)
    hr.add_period(1)
    hr.add_period(1)
    hr.max_readings = 5
    hr.add_times(0,0.5) #True Pulse
    hr.add_times(1.1,1.5) #True Pulse
    hr.add_times(2.1,2.5)#True Pulse
    hr.add_times(3,3.5)#True Pulse
    hr.add_times(4,4.5)#True Pulse
    assert hr.prevoius_is_false_pulse == False
    assert hr.two_back_is_flase_pulse == False
    hr.get_period_smart()
    assert hr.prevoius_is_false_pulse == False
    assert hr.two_back_is_flase_pulse == False
    assert hr.period_vector[4] == 1
    hr.add_times(4.6,4.7) #False Pulse
    hr.get_period_smart()
    assert hr.prevoius_is_false_pulse == True
    assert hr.two_back_is_flase_pulse == False
    assert hr.period_vector[4] == 1
    hr.add_times(5.1,5.6)#True Pulse
    hr.get_period_smart()
    assert hr.prevoius_is_false_pulse == False
    assert hr.two_back_is_flase_pulse == True
    assert round(hr.period_vector[4],1) == 1.1
    hr.add_times(5.5,5.6)#False Pulse
    hr.get_period_smart()
    assert hr.prevoius_is_false_pulse == True
    assert hr.two_back_is_flase_pulse == False
    assert round(hr.period_vector[4],1) == 0.9
    hr.add_times(6.1,6.6)#True Pulse
    hr.get_period_smart()
    assert hr.prevoius_is_false_pulse == False
    assert hr.two_back_is_flase_pulse == True
    assert round(hr.period_vector[4],1) == 1
    hr.add_times(7.0,7.4)#True Pulse
    hr.get_period_smart()
    assert hr.prevoius_is_false_pulse == False
    assert hr.two_back_is_flase_pulse == False
    assert round(hr.period_vector[4],1) == 0.9

def test_get_heartrate():
    hr = Heartrate(-1)
    hr.period_vector = [0]*5
    hr.add_period(1)
    hr.add_period(1)
    hr.add_period(1)
    hr.add_period(1)
    hr.add_period(1)
    assert round(hr.get_heartrate()) == 60
    hr.add_period(0.92)
    hr.add_period(0.9)
    hr.add_period(0.88)
    hr.add_period(0.86)
    hr.add_period(0.88)
    assert round(hr.get_heartrate()) == 68
    hr.add_period(0.65)
    hr.add_period(0.68)
    hr.add_period(0.65)
    hr.add_period(0.64)
    hr.add_period(0.62)
    assert round(hr.get_heartrate()) == 93

def counter_converter(quater_minute_count):#converts the count of heart beats in 15 seconds to one minute
    """
    converts the count of heart beats in 15 seconds to one minute
    
    >>> counter_converter(-1) 
    0
    >>> counter_converter(2)
    8
    >>> counter_converter(4)
    16
    >>> counter_converter(0)
    0
    """
    if quater_minute_count < 0:
        return 0
    return quater_minute_count*4

def test_counter_converter_negative():
    print 'test_counter_converter_negative'
    assert counter_converter(-1) == 0

def test_counter_converter_4():
    print 'test_counter_converter_4'
    assert counter_converter(4) == 16

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 22 09:28:56 2014

@author: Dawid
"""
import serial, time

class Heartrate:
    rising_edge_time = [0]
    falling_edge_time = [0]
    period_vector = [0]
    total_readings = 0          #the number of redings in the list
    readings_since_last_valid_period = 0
    temp_period = 0
    max_readings = 0
    prevoius_is_false_pulse = False
    two_back_is_flase_pulse = False
    ser = None
    
    def __init__(self,max_readings):
        if(max_readings > 0):
            self.serinit()
            self.max_readings = max_readings
            self.rising_edge_time = [0]*max_readings
            self.falling_edge_time = [0]*max_readings
            self.period_vector = [0]*max_readings
            print("Ininitilizing the reading of the heart rate with "+str(self.max_readings)+" readings.")
            self.initiate_period_vector()
            while self.total_readings < self.max_readings:
                print("Measuring value "+str(self.total_readings+1)+" of "+str(self.max_readings))
                self.measure_rise_and_fall()
                self.total_readings = self.total_readings+1
                self.get_period_smart()
            
    def add_times(self,rising_edge,falling_edge):
        """
        Handles the storage of values in the self.rising_edge_time and self.falling_edge_time lists
        """
        self.rising_edge_time.pop(0)
        self.falling_edge_time.pop(0)
        
        self.rising_edge_time.append(rising_edge)
        self.falling_edge_time.append(falling_edge)
    
    def add_period(self,period):
        """
        Halndels the storing of values in the self.period_vector
        """
        self.period_vector.pop(0)
        self.period_vector.append(period)
    
    def measure_rise_and_fall(self):
        """
        Measures the firts rising edge and the falling edge after thar rising edge and stores the values in
        the self.rising_edge_time and self.falling_edge_time lists by calling self.add_times()
        """
        rising_edge_time = None
        falling_edge_time = None
        self.wait_rising_edge()
        rising_edge_time = time.time()
        self.wait_falling_edge()
        falling_edge_time = time.time()
        self.add_times(rising_edge_time,falling_edge_time)
        self.readings_since_last_valid_period = self.readings_since_last_valid_period + 1
        
    def initiate_period_vector(self):
        """
        populates the self.period vector with values after the initial redings af rise and fall times was taken
        """
        self.add_period(1)
        
    def get_period_primative(self,position_1,position_2):
        """
        Calculates the time interval between 2 rising edges. At a specific position
        The number given as position is 0 for most recent , 1 for the secondmost recent and so forth.
        Position 1 should be smaller than position 2 in other words the fist position should be the more recent reading and position 2 should be the oldest reading.
        """
        if position_1 >= self.max_readings or position_2 <= position_1:
            return None
        return (self.rising_edge_time[self.max_readings-1-position_1] - self.rising_edge_time[self.max_readings-1-position_2])
    
    def get_high_time(self,position):
        """
        Gets the high time of the pulse. Accept a position attribute. 0 is the last pulse, 1 is the previous pulse, 2 is 2 pulses back and so forth.
        """
        return self.falling_edge_time[self.max_readings-1-position]-self.rising_edge_time[self.max_readings-1-position]
        
    def get_period_smart(self):
        """
        Uses the results of heartrate.get_period_primative() and finds the true period eliminating false peaks
        """
        period = None
        
        
        if  self.get_high_time(1)/self.get_high_time(0) < 1.5 and self.get_high_time(1)/self.get_high_time(0) > 0.5:
            period = (self.get_period_primative(0,1))
            self.two_back_is_flase_pulse = self.prevoius_is_false_pulse         
            self.prevoius_is_false_pulse = False
            
        else:
            if (self.two_back_is_flase_pulse==True and self.prevoius_is_false_pulse==False)or(self.two_back_is_flase_pulse==False and self.prevoius_is_false_pulse==True):
                period = (self.get_period_primative(0,2))
                self.two_back_is_flase_pulse = self.prevoius_is_false_pulse                
            if self.get_high_time(1)/self.get_high_time(0) > 1.6:
                self.prevoius_is_false_pulse = True
            else:
                self.prevoius_is_false_pulse = False
            

        if period>2 or period<0.4:
            pass
        else:
            self.add_period(period)

    def get_heartrate(self):
        return 60/(sum(self.period_vector)/len(self.period_vector))
    
    def serinit(self):
        """
        This function initiates the serial connection. It gets the COM number to use from get_COM_number()
        """
        com = 'COM'+str(self.get_COM_number())
        self.ser = serial.Serial(com,
                           baudrate=9600,
                           bytesize=serial.EIGHTBITS,
                           parity=serial.PARITY_NONE,
                           stopbits=serial.STOPBITS_ONE,
                           timeout=1,
                           xonxoff=0,
                           rtscts=0
                           )
                           
    def get_COM_number(self):
        """
        This functions handles the user input of the COM port number.
        A integer type is returned.
        """
        port = None
        valid_input=False
        print ("Please enter the COM port number to initiate the serial connection.")
        while not valid_input:
            port = raw_input('Enter port number:')
            try:
                int(port)
                valid_input = True
            except Exception:
                print "The value you entered is not a integer. Please enter again."
        return port
        
    def closeser(self):
        self.ser.close()

    def wait_rising_edge(self):
        if self.ser.getCTS():
            while self.ser.getCTS():
                pass
            while self.ser.getCTS()==False:
                pass
    
    def wait_falling_edge(self):
        while self.ser.getCTS()==False:
            pass
        while self.ser.getCTS():
            pass